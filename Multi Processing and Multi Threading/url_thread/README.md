# Excercise 1#

Problem Statement:
Fetch unique id from https://www.uuidgenerator.net/api/version1
Input=> 5

* output:
* ba42e844-87c5-11eb-8dcd-0242ac130003
* 284ebfb6-87c6-11eb-8dcd-0242ac130003
* 2b18f6e4-87c6-11eb-8dcd-0242ac130003
* 2d084978-87c6-11eb-8dcd-0242ac130003
* 2f0423dc-87c6-11eb-8dcd-0242ac130003

### Logic
* read user input
* create pool
* map the pool with our function which returns us the output of unique id
* using urllib open the link and read the response of the website
* Print the output and calculate total time

### How to run file? ###

* call the python file 'url_thread_01.py' file
