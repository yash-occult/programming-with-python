import threading
import time
import urllib
import requests
from multiprocessing import Pool

def get_data(n):
    url='https://www.uuidgenerator.net/api/version1'
    file = urllib.request.urlopen(url)
    for line in file:
        decoded_line = line.decode("utf-8")
        print(decoded_line)

if __name__ == '__main__':
    input_usr = int(input('Enter the number of times to call url: '))
    t = time.time()
    p = Pool(62)
    p.map(get_data,range(input_usr))
    print(time.time()-t)
