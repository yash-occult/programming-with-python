from duplicates_run_01 import run
input_file = open("test_case.txt", "r")
list_of_input = [(line.strip()).split() for line in input_file]
input_file.close()
total_inputs= sum(1 for line in open('test_case.txt'))
output = open("output.txt", 'a')
# print(list_of_input[0][0])
with open('test_case.txt', 'r'):
    for i in range(total_inputs):
        func_input = tuple(map(int,list_of_input[i][0].replace('[','').replace(']','').split(',')))
        func_output = run(func_input)
        if list_of_input[i][2] == func_output:
            output.write('Correct\n')
        else:
            output.write('Wrong\n')


