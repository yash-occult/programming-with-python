# Excercise 4 #

Problem Statement:
Write a Python program to extract the year, month, date, and time using Lambda.

### Logic? ###

* Fetch inputs from test_case.txt
* split the input string from '=>' character
* import extract_date from date_extract_04_run.py
* pass the string to extract_date function 
* using lambda function create individual variables of year,month,date,hour,minute,seconds
* create the string of output and compare it with the true outputs

### How to run file? ###

* call the python file 'date_extract_04_test.py' file
* check its output in output.txt file