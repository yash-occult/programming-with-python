from date_extract_04_run import extract_date
input_file = open("test_case.txt", "r")
list_of_input = [(line.strip()).split('=>') for line in input_file]
input_file.close()
total_inputs= sum(1 for line in open('test_case.txt'))
output = open("output.txt",'a')
with open('test_case.txt','r'):
    for i in range(total_inputs):
        correct_output = str(list_of_input[i][0]).replace('-',' ').replace(':',' ').strip(' ')
        function_output = extract_date(list_of_input[i][0]).strip(' ')
        if function_output == correct_output:
            print('correct')
            output.write('Correct\n')
        else:
            print('wrong')
            output.write('Wrong\n')

