from datetime import datetime
def extract_date(input):
    input_date = datetime.strptime(input.strip(' \t\r\n'), '%Y-%m-%d %H:%M:%S.%f')
    year = lambda x: x.year
    month = lambda x: x.month
    date = lambda x: x.day
    hour = lambda x: x.hour
    minute = lambda x: x.minute
    seconds = lambda  x:x.second
    microsecond = lambda x: x.microsecond
    second = float(str(seconds(input_date)) +'.'+str(microsecond(input_date)))
    extract = str(str(year(input_date))+" "+str(month(input_date))+" "+str(date(input_date))+" "+str(hour(input_date))+" "+str(minute(input_date))+" "+str(second))
    return extract

