# Excercise 3 #

Problem Statement:
Write a Python program to filter(even and odd) a list of integers using Lambda.

### Logic? ###

* Fetch inputs from file
* Convert String to List
* Remove '=>' from test_case.txt split the inputs
* import the run function from odd_even_03_run.py file
* pass-in the list of numbers, using lambda function
* create two variables namely even and odd
* pass the list into the lambda function and using filter fetch the necessary input to even and odd resp.
* return the two variable even and odd 
* check whether test_case.txt output matches function output and print result accordingly

### How to run file? ###

* call the python file 'odd_even_03_test.py' file
* check its output in output.txt file