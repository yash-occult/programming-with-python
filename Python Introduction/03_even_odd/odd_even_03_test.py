from odd_even_03_run import run
input_file = open("test_case.txt", "r")
list_of_input = [(line.strip()).split('=>') for line in input_file]
input_file.close()
total_inputs= sum(1 for line in open('test_case.txt'))
output = open("output.txt",'a')
with open('test_case.txt','r'):
    for i in range(total_inputs):
        func_input = list(map(int,list_of_input[i][0].replace('[','').replace(']','').split(',')))
        actual_even = list(map(int, list_of_input[i][1].replace('[', '').replace(']', '').split(',')))
        actual_odd = list(map(int, list_of_input[i][2].replace('[', '').replace(']', '').split(',')))
        even,odd = run(func_input)
        if actual_even == even and actual_odd == odd:
            print('correct')
            output.write('Correct\n')
        else:
            print('wrong')
            output.write('Wrong\n')

