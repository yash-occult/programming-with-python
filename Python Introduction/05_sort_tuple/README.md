# Excercise 5 #

Problem Statement:
Write a Python program to get a list, sorted in increasing order by the last element in each tuple from a given list of non-empty tuples.

### Logic? ###

* Fetch inputs from file
* Convert String to List of tuples
* Remove '=>' from test_case.txt split the inputs
* import the run function from sort_tuple_05_run.py file
* pass the input list to function run()
* Sort tuple based on increasing order of 1st index of every tuple using the function get_key()
* return the list of tuple and compare the function output to the actual answer

### How to run file? ###

* call the python file 'sort_tuple_05_test.py' file
* check its output in output.txt file