from sort_tuple_05_run import run
input_file = open("test_case.txt", "r")
list_of_input = [(line.strip()).split('=>') for line in input_file]
input_file.close()
total_inputs= sum(1 for line in open('test_case.txt'))
output = open("output.txt",'a')

with open('test_case.txt','r'):
    for i in range(total_inputs):
        function_input = [eval(ele) for ele in list(list_of_input[i])]
        correct_answer = function_input[1]
        function_output = run(function_input[0])
        if function_output == correct_answer:
            print('correct')
            output.write('Correct\n')
        else:
            print('wrong')
            output.write('Wrong\n')

