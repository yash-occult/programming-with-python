# Excercise 2 #

Problem Statement:
Write a Python program to add two positive integers in tuple without using the '+' operator.

### Logic? ###

* Fetch inputs from file
* take the tuple in list input
* convert tuple to list 
* call the function run from add_run_02.py
* using list built-in function 'sum' sum up all the elements of list
* return output and append to output.txt

### How to run file? ###

* call the python file 'add_test_02.py' file
* check its output in output.txt file