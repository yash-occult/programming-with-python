from add_test_02 import run
input_file = open("test_case.txt", "r")
list_of_input = [(line.strip()).split() for line in input_file]
input_file.close()
total_inputs= sum(1 for line in open('test_case.txt'))
output = open("output.txt",'a')
with open('test_case.txt','r'):
    for i in range(total_inputs):
        func_input = tuple(map(int,list_of_input[i][0].replace('(','').replace(')','').split(',')))
        func_output = run(func_input)
        if int(list_of_input[i][2]) == func_output:
            output.write('Correct\n')
        else:
            output.write('Wrong\n')


