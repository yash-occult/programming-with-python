# Excercise 1#

Problem Statement:
Scrape website data and perform analysis on the website data.

### Logic? ###

* Used selenium to scrape forbes website data
* Created a driver for selenium to interact with its functions.
* Searched for classes and x_paths that are useful to scrape.
* Created 6 list of elements that need to be scraped.
* Converted the scraped data to dictionary and created csv using pandas.
* Using plotply(Graphing library), pandas analysed the data
* Removed null values from csv.
* Converted age which was in str data type to integer data type


### Questions and analysis found from the website data ###
* Which are the industires that produces billionaire?
* Which Industries produce most billionaire?
* Which Countries have most billionaire?
* Which age group has the highest billionaire count?
* Does individual fields have age biases?

## Analysis made ##
### industires that produces billionaire ###
* Finance & Investments         350
* Technology                    328
* Manufacturing                 295
* Fashion & Retail              252
* Healthcare                    203
* Real Estate                   199
* Food & Beverage               188
* Diversified                   168
* Energy                         94
* Media & Entertainment          86
* Metals & Mining                72
* Automotive                     64
* Service                        63
* Construction & Engineering     37
* Logistics                      35
* Telecom                        30
* Gambling & Casinos             24
* Sports                         24

### Industries producing most billionaire ###
* Finance & Investments
* Technology
* Manufacturing
* Fashion & Retail
* Healthcare
### Countries that produce most Billionaire ###
* United States
* China
* India
* Germany
* Russia

### Age group that has most Billionaire ###
#### Looking at the age group, Age 55-59 has 407 Billionaires ####

* It takes time to be billionaire
* Very few young age people are billionaire
* As age increases the number of billionaire decreases, possible reason might be stepping down from their positions in companies, Donations
* Internet came in 1983 which was 38 years ago possible reason why there are more people below age 59 who are billionaire


### Does individual industires have biases in them with respect to age ###
#### For technology  field ####
* From the histogram below, Technology is an industry with diversification of age. 
* Young to old age groups are intersted in investing in this field.
* Hence, this field is competitive and broad.
#### For Finance and Investments ####
* From the histogram, Most of the people in Finance & Investments are old which shows that this field requires time and dedication to grow.
* Age group of 60-64 shows the peak in our histogram.
* Young agr groups do not seem to enter this field as it requires expirence and knowledge.
### How to run file? ###

* Call the python file 'Scrape_analysis_01.py' file to create csv file
* Run the Exploratory Data Analysis Forbes.ipynb to see the analysis.
