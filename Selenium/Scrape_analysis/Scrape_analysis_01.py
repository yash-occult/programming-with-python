from selenium import webdriver
import pandas as pd
import time

# defining path of chrome driver
driver = webdriver.Chrome(executable_path=r'C:\Users\Weboccult\Downloads\chromedriver_win32\chromedriver.exe')

# site to fetch data from
driver.get('https://www.forbes.com/billionaires/')

# adding delay, due to slow load list return empty
time.sleep(2)

# number of pages
numberOfPages = len(driver.find_elements_by_class_name('goto-page__btn'))
rank, name, net_worth, age, country, industry = [], [], [], [], [], []

for _ in range(numberOfPages-1):
    time.sleep(3) # to add delay for page load
    try:
        ranks = driver.find_elements_by_class_name('rank')  # Get rank of person
        rank += [x.text for x in ranks[1:]]  # used slicing 'Rank' Keyword gets added every new page

        names = driver.find_elements_by_xpath('//*[@class="personName"]/div')  # Get name of person
        name += [name.text for name in names]  # separating name object to text

        net_worths = driver.find_elements_by_xpath('//*[@class="netWorth"]/div')  # Get net worth of person
        net_worth += [net_worth.text for net_worth in net_worths]  # separating net_worth object to text

        ages = driver.find_elements_by_xpath('//*[@class="age"]/div')  # Get age of person
        age += [age.text for age in ages]  # separating names age to text

        countries = driver.find_elements_by_class_name('country')  # Get country of person
        country += [country.text for country in countries[1:]]  # slicing 'countyr' Keyword gets added every new page

        industries = driver.find_elements_by_xpath('//*[@class="category"]/div')  # Get industry of person
        industry += [industry.text for industry in industries]  # separating industry object to text

    except:
        pass

    # to switch to next page
    driver.find_element_by_class_name('pagination-btn--next').click()

#  creating dictionary of list
dictonary_of_list = {'Rank': rank, 'Name': name, 'Net Worth': net_worth,
                     'Age': age, 'Country': country, 'Industry': industry}

# creating dataframe using dictionary
df = pd.DataFrame(dictonary_of_list)

# saving the dataframe
df.to_csv('test.csv')
