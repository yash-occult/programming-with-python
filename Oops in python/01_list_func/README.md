# Excercise 1#

Problem Statement:
Write a Class which has all functionality of a list.

### Logic? ###

* Create two classes, One for handling Exception and another as CustomList
* Override Append Method and check whether the number is divisible by 5 
* if number not divisible by 5 then raise an Exception by calling the CustomException() class
* For the function to continue even after raising Exception add pass to Except.
* Create Obj of class CustomList() and access the class append
* Another method is_all_even() to check if all the list elemnent are even or not using all function.

### How to run file? ###

* call the python file 'list_func_01_run.py' file
