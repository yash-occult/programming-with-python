# Excercise 2#

Problem Statement:
Write a program to demonstrate multiple inheritance

### Logic? ###

* create parent class named Animal() 
* create sub class names Wild(),Pet(),Canine(),Feline()
* create sub class with the following inheritance
* Wild => Leapord(),FoX()
* Pet => Dog(),Cat()
* Canine => Dog(),Fox()
* Feline => Cat(),Leapord()
* On calling the child class, Should access all the parent class with their messages

### How to run file? ###

* call the python file 'multiple_inheritance_02.py' file
