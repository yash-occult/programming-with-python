class Animal:
    __message = 'I am animal'
    def leg(self):
        print('I have Four Legs')
    def gettr(self):
        return self.__message

class Wild(Animal):
    __message = 'I am a wild Animal'
    
    def food(self):
        print('Eat wild food')
    def gettr(self):
        return Animal.gettr(self), Wild.__message

class Pet(Animal):
    __message = 'I am a pet Animal'

    def food(self):
        print('Eat pet food')

    def gettr(self):
        return Animal.gettr(self), self.__message


class Canine(Animal):
    __message = 'I am a canine Animal'

    def food(self):
        print('Eat canine food')

    def gettr(self):
        return Animal.gettr(self), self.__message


class Feline(Animal):
    __message = 'I am a feline Animal'

    def food(self):
        print('Eat feline food')

    def gettr(self):
        return Animal.gettr(self), self.__message

class Leapord(Wild,Feline):
    __message = 'I am Leapord'

    def voice(self):
        print('leapord Voice')
    def gettr(self):
        return Wild.gettr(self), Feline.gettr(self)[1],self.__message

class Fox(Wild,Canine):
    __message = 'I am Fox'

    def voice(self):
        print('Fox Voice')
    def gettr(self):
        return Wild.gettr(self), Canine.gettr(self)[1],self.__message

class Dog(Pet,Canine):
    __message = 'I am Dog'

    def voice(self):
        print('Dog Voice')
    def gettr(self):
        return Pet.gettr(self), Canine.gettr(self)[1],self.__message

class Cat(Pet,Feline):
    __message = 'I am Cat'

    def voice(self):
        print('Cat Voice')
    def gettr(self):
        return Pet.gettr(self), Feline.gettr(self)[1],self.__message

animal_obj = Animal()
wild_obj = Dog()
print(wild_obj.gettr())
