import time
import threading
import  multiprocessing
t = time.time()
user_input = 1000
input_range = (2* user_input) +2
n= input_range//2

def if_func(n):
    for i in range(-n, n+1):
        for j in range(-n, n+1):
            if abs(i) == n or abs(j) == n or i == 0 and j == 0:
                print('0', end=' ')

            elif (j < 0 and i < 0 or j > 0 and i > 0) and abs(j) <= abs(i):
                print(abs(j), end=' ')

            elif (j > 0 and i < 0 or j < 0 and i > 0) and abs(j) >= abs(i):
                print(abs(j), end=' ')

            else:
                print(' ', end=' ')
        print(' ', end='\n')
if __name__ == '__main__':
    # a_pool = multiprocessing.Pool()
    # a_pool.map(if_func, range(1000,1001))
    t1 = multiprocessing.Process(target=if_func,args=(n,))
    # a_pool.close()
    # a_pool.join()
    t1.start()
    t1.join()
    print(time.time()-t)