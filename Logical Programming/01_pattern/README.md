# Excercise 1#

Problem Statement:
Draw following pattern for n=5
Input=> 5

*  Output=>
 * 0 0 0 0 0 0 0 0 0 0 0 0 0 
 * 0 5 4 3 2 1           5 0 
  0   4 3 2 1         4 5 0
  0     3 2 1       3 4 5 0
  0       2 1     2 3 4 5 0
  0         1   1 2 3 4 5 0
  0           0           0
  0 5 4 3 2 1   1         0
  0 5 4 3 2     1 2       0
  0 5 4 3       1 2 3     0
  0 5 4         1 2 3 4   0
  0 5           1 2 3 4 5 0
  0 0 0 0 0 0 0 0 0 0 0 0 0

### How to run file? ###

* call the python file 'list_func_01_run.py' file
